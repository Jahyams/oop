<?php

    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $sheep = new animal("Shaun");

    echo "Name = " .$sheep->name ."<br>"; // "shaun"
    echo "Legs = " .$sheep->legs ."<br>"; // 4
    echo "Cold Blooded = " .$sheep->cold_blooded ."<br><br>"; // "no"

    $kodok = new Frog ("Buduk");
    echo "Name = " .$kodok->name ."<br>"; // "Frog"
    echo "Legs = " .$kodok->legs ."<br>"; // 4
    echo "Cold Blooded = " .$kodok->cold_blooded ."<br>"; // "no"
    echo "Jump = " .$kodok->jump ."<br><br>"; // "no"

    $sungokong = new Ape ("Kera Sakti");
    echo "Name = " .$sungokong->name ."<br>"; // "Ape"
    echo "Legs = " .$sungokong->legs ."<br>"; // 2
    echo "Cold Blooded = " .$sungokong->cold_blooded ."<br>"; // "no"
    echo "Yell = " .$sungokong->yell ."<br>"; // "no"

?>