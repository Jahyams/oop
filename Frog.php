<?php
require_once('animal.php');

class Frog extends animal{
    public $legs = 4;
    public $cold_blooded = "No";
    public $jump = "Hop Hop";
}
?>