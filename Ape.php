<?php
require_once('animal.php');

class Ape extends animal{
    public $legs = 2;
    public $cold_blooded = "No";
    public $yell = "Auooo";
}
?>